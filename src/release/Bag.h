/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Bag.h
 * @authors Frank M. Carrano
 *          Timothy M. Henry
 *			Maureen Holley <holley333@live.missouristate.edu>
 * @brief  Bag interface specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef HW01_BAG_H
#define HW01_BAG_H

#include <vector>

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

    /**
     * @brief Bag Interface introduced in Chapter 1 in Listing 1-1 with
     * modifications by Jim Daehn.
     * @tparam T class template parameter; the type of element stored in this
     * `Bag`.
     */
    template <typename T>
    class Bag {
    public:
        /**
         * @brief Gets the current number of entries in this bag.
         * @return The integer number of entries currently in this `Bag` is
         * returned.
         */
        virtual int getCurrentSize() const = 0;

        /**
         * @brief Determine whether this `Bag` is empty.
         * @return  True if this `Bag` is empty; false otherwise.
         */
        virtual bool isEmpty() const = 0;

        /**
         * @brief Adds a new entry to this `Bag`.
         * @post If successful, `newEntry` is stored in the bag and the count
         * of items in the bag has increased by 1.
         * @param newEntry The object to be added as a new entry.
         * @return True if addition was successful, or false if not
         */
        virtual bool add(const T& newEntry) = 0;

        /**
         * @brief Removes one occurrence of a given entry from this `Bag` if
         * possible.
         * @post If successful, `anEntry` has been removed from the bag and
         * the count of items i the bag has decreased by 1.
         * @param anEntry The entry to be removed.
         * @return True if removal was successful, or false if not.
         */
        virtual bool remove(const T& anEntry) = 0;

        /**
         * @brief Removes all entries from this bag.
         * @post Bag contains no items, and the count of items is 0.
         */
        virtual void clear() = 0;

        /**
         * @brief Counts the number of times a given entry appears in this bag.
         * @param anEntry The entry to be counted.
         * @return The number of times `anEntry` appears in the bag.
         */
        virtual int getFrequencyOf(const T& anEntry) const = 0;

        /**
         * @brief Tests whether this bag contains a given entry.
         * @param anEntry The entry to locate.
         * @return True if bag contains `anEntry`, or false otherwise.
         */
        virtual bool contains(const T& anEntry) const = 0;

        /**
         * @brief Empties and then fills a given `std::vector` with all
         * entries that are in this `Bag`.
         * @return A `std::vector` containing copies of all the entries in
         * this `Bag`.
         */
        virtual std::vector<T> toVector() const = 0;
		
		/**
		* @brief Takes two bags and creates a new bag and combines
		* elements from the first two bag
		* @param bagA which contains a series of elements
		* @return A third bag that is the union of two bags
		* @post Bag and bagA aren't changed
		*/
		virtual Bag<T> unionOp(const Bag<T>, bagA) const {
			//Need to return a new bag when we're done
			Bag<T> bagB;
			
			//Get the elements of this Bag using the toVector operation
			std:: vector bag_Elements = toVector();
		
			//Add elements of Bag to bagB
			for (T element : bag_Elements) {
				bagB.add(element)
			}
			
			//Get the elements of bagA using toVector and add
			std::vector bagB_Elements = bagA.toVector();
			for (T element : bagB_Elements {
				bagB.add(element);
			}
			
			//bagB has all the elements we need
			return bagB;
		}
				
		/**
		* @brief Takes two bags and creates a new bag from things that are the same
		* in the elements of the first two bag
		* @param bagA which contains a series of elements
		* @return A third bag that is the intersection of two bags
		* @post Bag and bagA aren't changed
		*/
		virtual Bag<T> intersection(const Bag<T>, bagA) const;
			//Need to return a new bag when we're done
			Bag<T> bagB;
			
			//Get the elements of this Bag using the toVector operation
			std:: vector bag_Elements = toVector();
			
			//Make a loop that checks the frequency in bagB with the
			//frequency of bagB and adds it to bagB
			for (T element : bag_Elements){
				if (bagB.getFrequencyOf(element) < bagA.getFrequencyOf(element){
					bagB.add(element);
					
			//returns bagB
			return bagB;

		/**
		* @brief Takes two bags and creates a new bag from things that are not the same
		* of the elements of the first two bag
		* @param bagA which contains a series of elements
		* @return A third bag that is the difference of two bags
		* @post Bag and bagA aren't changed
		*/
		virtual Bag<T> difference(const Bag<T>, bagA);
			//Need to return a new bag when we're done
			Bag<T> bagB;
			
			//Get the elements of this Bag using the toVector operation
			std:: vector bag_Elements = toVector();
			
			//Make a loop that adds Bag into BagB and
			//checks the frequency in bagB with the
			//frequency and subtracts it and then adds it to bagB
			for (T element : bag_Elements){
				bagB.add(elements);
				if (bagB.getFrequencyOf(element) <= bagA.getFrequencyOf(element){
					bagB.remove(element);
					
			//returns bagB
			return bagB;

        /**
         * @brief Destroys this bag and frees its assigned memory.
         */
        virtual ~Bag() {
            // inlined, no-op
        }
    }; // end Bag
} // end namespace csc232

#endif //HW01_BAG_H
